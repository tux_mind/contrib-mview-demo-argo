# README
This repository contains an OpenMP implementation of the Stereo-Matching application integrated with BarbequeRTRM ( [http://bosp.dei.polimi.it](http://bosp.dei.polimi.it) ) and with the ARGO framework ( [https://bitbucket.org/davide_gadioli/external-argo](https://bitbucket.org/davide_gadioli/external-argo) ).

The algorithm is described in the article:
> Ke Zhang, Jiangbo Lu, and Gauthier Lafruit, "Cross-Based Local Stereo Matching Using Orthogonal Integral Images", IEEE Transactions on Circuits and Systems for Video Technology, Vol.19, pp.1073-1079.

## What is this repository for?
This application computes a disparity map on an input pair of stereo images. The algorithm exposes a set of application-specifc parameters that impact on both the execution time and the accuracy of the result. Those parameters are selected automatically at Run-Time by the ARGO Framework. However is possible to tune the behaviour of the ApplicationSpecific Run-Time Manager that select the parameters that best fit with the application requirements and preferences.

## How do i get set up?

First is required to clone the framework ARGO in the BOSP directory, at the path <BOSP_ROOT>/external/optional, and renamed into argo. The second step is to clone this repository at the path <BOSP_ROOT>/contrib/user, and renamed into mview-demo-argo. Thus the final local repositories should be deployed as follows:
> <BOSP_ROOT>/external/optional/argo

> <BOSP_ROOT>/contrib/user/mview-demo-argo

then the procedure to compile the application is:
~~~
:::bash
$ cd <BOSP_ROOT>
$ make bootstrap
$ make menuconfig
~~~
In the menuconfig select the application under the menu Applications->OpenMP-MView-ARGO. Save the configuration and contine with the command:
~~~
:::bash
$ make
~~~

In order to execute the application launch the following script:
> <BOSP_ROOT>/contrib/user/mview-demo-argo/startDemo.sh


Note:
> The dataset Tsukuba used in demo is very large (~5GB) so it must be downloaded manually from the official site of the University of Tsukuba. At the end of the building step, is shoed a mini how-to that explain how to retrieve the dataset and how to deploy it


## Contribution guidelines
Clone the repository and send pull requests, any contribution is welcome.


## Who do I talk to?
Contact: davide [dot] gadioli [at] polimi [dot] it

Organization: Politecnico di Milano, Italy