/**
 *       @file  MviewDemoMost_loader.h
 *      @brief  The OpenMP-MView-ARGO BarbequeRTRM application
 *
 * Description: OpenMP implementation of the Stereo-Matching algorithm described
 *              in the article: Ke Zhang, Jiangbo Lu and Gauthier Lafruit,
 *              "Cross-Based Local Stereo Matching Using Orthogonal Integral
 *              Images", IEEE Transactions on Circuits and Systems for Video
 *              Technology, Vol. 19, no. 7, July 2009.
 *
 *     @author  Edoardo Paone, edoardo.paone@polimi.it
 *              Davide Gadioli, davide.gadioli@polimi.it
 *
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 2014, Politecnico di Milano
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#ifndef MVIEW_DEMO_ARGO_LOADER_H_
#define MVIEW_DEMO_ARGO_LOADER_H_

#include <mutex>
#include <fstream>
#include <vector>


#include <opencv/cv.h>
#include <opencv/highgui.h>


#include <img_buffer.h>


extern bool exit_loader;

void loadImage(
	std::string name,
	ImgBufferPtr buffer,
	int exc_id = 0
);




#endif // MVIEW_DEMO_ARGO_LOADER_H_

