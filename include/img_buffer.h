/**
 *       @file  img_buffer.h
 *      @brief  The OpenMP-MView-ARGO BarbequeRTRM application
 *
 * Description: OpenMP implementation of the Stereo-Matching algorithm described
 *              in the article: Ke Zhang, Jiangbo Lu and Gauthier Lafruit,
 *              "Cross-Based Local Stereo Matching Using Orthogonal Integral
 *              Images", IEEE Transactions on Circuits and Systems for Video
 *              Technology, Vol. 19, no. 7, July 2009.
 *
 *     @author  Edoardo Paone, edoardo.paone@polimi.it
 *              Davide Gadioli, davide.gadioli@polimi.it
 *
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 2014, Politecnico di Milano
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#ifndef MVIEW_DEMO_ARGO_IMG_BUFFER_H
#define MVIEW_DEMO_ARGO_IMG_BUFFER_H

// ------ System include
#include <vector>
#include <string>
#include <mutex>
#include <memory>


// ------ OpenCV library
#include <opencv/cv.h>
#include <opencv/highgui.h>


// ------ Status typedef


// Proximity level
enum class ProximityLevel: uint_fast8_t {
	Far   = 255,
	Near  = 180,
	Close = 130
};


// Region of proximity
typedef std::vector< std::vector<uint_fast8_t> > proxRegions;


// Execution state
enum class ExecutionStatus:int_fast8_t {
	Running,
	Starving,
	Squeezing
};

// Application state
enum class ApplicationState:int_fast8_t {
	Normal,
	Danger
};




// ------ Img buffer class
class ImgBuffer {
	
public:
	ImgBuffer(
		std::string dataset,
		const unsigned int proximity_box_size,
		unsigned int starting_frame = 0
	);
	
	// load the new images from files
	void next();
	
	
	// ------ Getters
	
	// getter image geometry and type
	void getImageStat(int& width, int& height, int& type);
	
	// get the reference image ( load only for the first frame )
	void getFirstReferenceFrame(cv::Mat& reference);
	
	// Get the input images
	void getInputImages(cv::Mat& left, cv::Mat& right);
	
	// Get the proximity region stat
	void getProximityRegionStat(
		unsigned int& square_size,
		unsigned int& row_number,
		unsigned int& col_number
		);
	
	// Get the application stat
	void getOutput(ExecutionStatus& status, ApplicationState& state, proxRegions& proximity, cv::Mat& output);
	
	
	// ------ Setters
	
	// Set the application stat
	void setOutput(const ExecutionStatus status, 
		const ApplicationState state,
		const proxRegions proximity,
		const cv::Mat& output
		);
	
private:
	
	// ------ Utility functions
	std::string get_left_image_name( void );
	std::string get_right_image_name( void );
	
	
	// frame counter
	unsigned int frame_counter;
	
	// frame buffers
	cv::Mat left;
	cv::Mat right;
	cv::Mat out;
	
	// images stats
	int width;
	int height;
	int type;
	
	
	// application stats
	ExecutionStatus status;
	ApplicationState state;
	proxRegions regions;
	
	
	// lock for the image processing
	std::mutex buffer_mutex;
	
	
	// image iterator stuff
	std::string base_file_name;
	std::string dataset_name;
	std::string reference_img_name;
	unsigned int frame_number;
	
	
	// proximity region stuff
	const unsigned int square_size;
	unsigned int col_n;
	unsigned int row_n;
};


typedef std::shared_ptr<ImgBuffer> ImgBufferPtr;



#endif // MVIEW_DEMO_ARGO_IMG_BUFFER_H
